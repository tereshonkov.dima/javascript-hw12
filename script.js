// - Після натискання вказаних клавіш - та кнопка, на якій написана ця літера, повинна фарбуватися в синій колір. При цьому якщо якась інша літера вже раніше була пофарбована в синій колір - вона стає чорною. Наприклад за натисканням Enter перша кнопка забарвлюється у синій колір. Далі, користувач натискає S, і кнопка S забарвлюється в синій колір, а кнопка Enter знову стає чорною.

// Але якщо при натискані на кнопку її не існує в розмітці, то попередня активна кнопка повина стати неактивною.




document.body.addEventListener("keydown", function (event) {
    btn.forEach((element) => {
        if (event.key.toLowerCase() === element.textContent.toLowerCase()) {
            element.style.backgroundColor = "blue";
        } else if (event.key.toLowerCase() !== element.textContent.toLowerCase()) {
            element.style.backgroundColor = "black";
        }
    })
});